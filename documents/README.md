| Document | Description |
| ------ | ------ |
| [Journal_of_Applied_Crystallography_Vol_48_pp_1324.pdf](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/binoculars-documentation/-/blob/master/documents/Journal_of_Applied_Crystallography_Vol_48_pp_1324.pdf) | Data reduction and analysis for 2D detectors by Sander Roobol, Willem Onderwaater, Jacub Drnec and Joost Frenken - 2015 - [Source](https://journals.iucr.org/j/issues/2015/04/00/rg5089/index.html) | 
