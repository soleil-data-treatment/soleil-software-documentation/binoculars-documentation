# Aide et ressources de BINoculars pour Synchrotron SOLEIL

## Résumé
- réduction de données, transformation d'espace, visualisation 2D, intégration des F² le long des rod. 
- Open source

## Sources
- Code source: https://github.com/picca/binoculars
- Documentation officielle: Documentation obsolete et minimale.

## Navigation rapide
| Wiki SOLEIL | Fichiers téléchargeables | Tutoriaux | Page pan-data |
| ------ | ------ | ------ |  ------ |
| [Tutoriel d'installation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/binoculars-documentation/-/wikis/Setup-guide) | [Documents d'utilisation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/binoculars-documentation/-/tree/master/documents)| [Tutoriaux officiels](https://github.com/id03/binoculars/wiki) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/161/binoculars) |
| [Tutoriaux SOLEIL](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/binoculars-documentation/wikis/home) | [Jeux de données](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/binoculars-documentation/-/tree/master/dataset) |   |   |   |

## Installation
- Systèmes d'exploitation supportés: Linux, Machine virtuelle VirtualBox
- Installation: Facile (tout se passe bien), OK sur Debian, mais pas autre. Traitement à distance souhaitable.

## Format de données
- en entrée: NXS (HDF5)
- en sortie: HDF5
- Ruche locale
